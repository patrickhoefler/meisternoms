import fetch from 'isomorphic-fetch'

export const REQUEST_MENU = 'REQUEST_MENU'
export const RECEIVE_MENU = 'RECEIVE_MENU'

function requestMenu() {
  return {
    type: REQUEST_MENU
  }
}

function receiveMenu(json) {
  return {
    type: RECEIVE_MENU,
    menuItems: json.sections,
    receivedAt: Date.now()
  }
}

function fetchMenu() {
  return dispatch => {
    dispatch(requestMenu())
    return fetch('http://meisternomsapi.patrickhoefler.net.local/menu.json')
      .then(
        response => response.json()
      )
      .then(
        json => dispatch(receiveMenu(json))
      )
  }
}

function shouldFetchMenu(state) {
  const menu = state.menu
  if (!menu) {
    return true
  } else if (menu.isFetching) {
    return false
  }
}

export function fetchMenuIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchMenu(getState())) {
      return dispatch(fetchMenu())
    }
  }
}
