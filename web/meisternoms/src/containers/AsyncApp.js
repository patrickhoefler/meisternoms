import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {fetchMenuIfNeeded} from '../actions'
import Menu from '../components/Menu'

import logo from '../logo.svg';
import '../App.css';

class AsyncApp extends Component {
  componentDidMount() {
    const {dispatch} = this.props
    dispatch(fetchMenuIfNeeded())
  }

  render() {
    const {menuItems, isFetching} = this.props
    return (

      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <h2>MeisterNoms</h2>
        </div>

        <div className="App-intro">
          {isFetching && menuItems.length === 0 && <h2>Loading...</h2>}
          {menuItems.length > 0 && <div>
            <Menu menuItems={menuItems}/>
          </div>
}
        </div>
      </div>
    )
  }
}

AsyncApp.propTypes = {
  menuItems: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const isFetching = state.menuItems.isFetching
  const menuItems = state.menuItems.items

  return {menuItems, isFetching}
}

export default connect(mapStateToProps)(AsyncApp)
