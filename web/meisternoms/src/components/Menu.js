import React, { Component } from 'react';

class Menu extends Component {
  render() {
    return (
      <div>
        {this.props.menuItems.map(function(section) {
          return (
            <div key={section.name}>
              <h3>{section.name}</h3>
              <ul>
                <SectionItems items={section.items}/>
              </ul>
            </div>
          )
        })}
      </div>
    );
  }
}

class SectionItems extends Component {
  render() {
    return (
      <div>
        {this.props.items.map(function(item) {
          return (
            <li key={item.product_id}>{item.name}</li>
          )
        })}
      </div>
    );
  }
}

export default Menu
