import { combineReducers } from 'redux'
import {
  REQUEST_MENU, RECEIVE_MENU
} from './actions'

function menuItems(state = {
  isFetching: false,
  items: []
}, action) {
  switch (action.type) {
    case REQUEST_MENU:
      return Object.assign({}, state, {
        isFetching: true
      })
    case RECEIVE_MENU:
      return Object.assign({}, state, {
        isFetching: false,
        items: action.menuItems,
        lastUpdated: action.receivedAt
      })
    default:
      return state
  }
}

const rootReducer = combineReducers({
  menuItems
})

export default rootReducer
