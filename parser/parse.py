#!/usr/bin/env python3

from bs4 import BeautifulSoup, Tag
import requests

# menu_url = 'https://www.ritabringts.at/speisekarte/09'
# menu_html = requests.get(menu_url).content

with open('tmp/rita_menu_09.html', 'rt') as menu_file:
    menu_html = menu_file.read()

soup = BeautifulSoup(menu_html, 'html.parser')

menu = {'sections': []}

for item in soup.find('div', {'class': 'view-content row'}).children:
    if isinstance(item, Tag):
        if item.name == 'h3':
            menu['sections'].append({
                'name': item.string.split()[0],
                'items': []
            })
        else:
            product_id_tag = item.find('input', {'name': 'product_id'})
            if product_id_tag is None:
                continue

            menu['sections'][-1]['items'].append({
                'name': item.find('a', {'class': 'store-item-title'}).string,
                'product_id': int(product_id_tag['value'])
            })

print(menu)
